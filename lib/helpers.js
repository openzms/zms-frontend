import { useNuxtApp } from '#app/nuxt'

export function getElementNameById(id) {
  let app = useNuxtApp()
  if (app.$clientStore.is_admin)
    return app.$adminStore.getElementNameById(id)
  else
    return app.$userStore.getElementNameById(id)
}

export function YesNo(val) {
  return (val ? "Yes" : "No");
}
