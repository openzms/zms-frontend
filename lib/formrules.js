
function required(v)
{
    return (v != '' && v != null) || 'required';
}

function nonzerolen(v)
{
    return typeof v !== 'string' ?
        true : (v.length ? true : "input must not be empty string");
}

function morethanspace(v)
{
    return (!v || typeof v !== 'string') ?
        true : (v.trim().length > 0 ? true: "input must not be only whitespace");
}

function positiveInteger(v)
{
    return v >= 0 || 'must be an integer >= zero';
}

function optionalPositiveInteger(v)
{
    return !v || positiveInteger(v)
}

function positiveNumber(v)
{
    return (typeof v === "number" && v > 0) || 'must be a positive number';
}

function optionalPositiveNumber(v)
{
    return !v || positiveNumber(v)
}

function validAzimuth(v)
{
    return (typeof v === "number" && v >= 0 && v <= 360) ||
	'must be a number from 0 to 360';
}
function validElevation(v)
{
    return (typeof v === "number" && v >= -90 && v <= 90) ||
	'must be a number from -90 to 90';
}

function validBandwidth(v)
{
    return (v == null || (typeof v === "number" && v > 0)) ||
        'Not a reasonable Bandwidth value';
}

function validEIRP(v)
{
    return (v == null || (v >= -200 && v <= 200)) ||
        'Not a reasonable EIRP value';
}

function validDate(v)
{
    var pattern1 = /(0\d{1}|1[0-2])\/([0-2]\d{1}|3[0-1])\/(19|20)(\d{2})/;
    var pattern2 = /(0\d{1}|1[0-2])\/([0-2]\d{1}|3[0-1])\/(19|20)(\d{2}) \d{2}:\d{2}:\d{2}/;
    return (v == null || v == "" || v.match(pattern1) || v.match(pattern2)) ? true : 'Not a valid date';
}

function validEmail(v)
{
    return v.match(/[a-zA-Z0-9\+\-_~\.]+@[a-zA-Z0-9\.:]+/) ? true : 'Invalid email address'
}

function validUUID(v)
{
    return v.match(/^[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}$/) ? true : 'Invalid email address'
}

function validUsername(v)
{
    return v ? true : "Username required"
}

function validPassword(v)
{
    return v ? true : "Password required"
}

function validLatitude(v)
{
    return (typeof v === "number" && v >= -90 && v <= 90) ||
	'must be a number from -90 to 90';
}
function validLongitude(v)
{
    return (typeof v === "number" && v >= -180 && v <= 180) ||
	'must be a number from -180 to 180';
}

function validFrequency(v)
{
    return (typeof v === "number" && v >= 0) ||
	'must be a number greater or equal to 0';
}

export function formRules()
{
    return {
        required: required,
        nonzerolen: nonzerolen,
        morethanspace: morethanspace,
        positiveInteger: positiveInteger,
        validEIRP: validEIRP,
        validDate: validDate,
        positiveNumber: positiveNumber,
        validEmail: validEmail,
        validUUID: validUUID,
	validAzimuth: validAzimuth,
	validElevation: validElevation,
        validUsername: validUsername,
        validPassword: validPassword,
        validLatitude: validLatitude,
        validLongitude: validLongitude,
        validFrequency: validFrequency,
        validBandwidth: validBandwidth,
    };
}
