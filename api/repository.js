// Provides a repo factory plugin with handler callbacks

import * as qs from 'qs'

export default $fetch => (resource) => ({
  list(params, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'GET',
      params: params
    }
    let r = $fetch(`/${resource}`, Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  create(data, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'POST',
      body: jsonSerializer(data)
    }
    let r = $fetch(`/${resource}`, Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  clone(id, data, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'POST',
      body: jsonSerializer(data)
    }
    let r = $fetch(`/${resource}/${id}`, Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  createSecondary(path, data, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'POST',
      body: jsonSerializer(data)
    }
    let r = $fetch(`/${resource}/${path}`, Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  read(id, params, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'GET',
      params: params
    }
    let r = $fetch(`/${resource}/${Array.isArray(id) ? id.join('/') : id}`,
                   Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  readSecondary(id, secondaryResource, params, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'GET',
      params: params
    }
    let r = $fetch(`/${resource}/${Array.isArray(id) ? id.join('/') : id}/${secondaryResource}`,
                   Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  update(id, data, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'PUT',
      body: jsonSerializer(data)
    }
    let r = $fetch(`/${resource}/${id}`, Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  delete(id, options = {}, handler = defaultHandler) {
    let lopts = {
      method: 'DELETE',
      // XXX: due to limitations in node.js 20.x fetch, when used
      // via undici (https://github.com/unjs/h3/issues/375,
      // https://github.com/unjs/h3/pull/605 ), we send a non-null
      // body for DELETE.
      body: {}
    }
    console.log("delete url", `/${resource}/${id}`)
    let r = $fetch(`/${resource}/${id}`, Object.assign({}, options, lopts))
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  }
})

function defaultHandler(e) {
  if (e.response) {
    console.log(e.response)
  }
  else if (e.request) {
    console.log(e.request)
  }
  else {
    console.log('Error', e.message, e)
  }
  //
  // There seems to be some lingering oddities where errors do not always
  // get bumped to the root vue error handler (and thus do not hit our
  // error.vue page).  Forcing uncaught errors to the error page is "always"
  // the right thing to do, so we make it happen.  The caller can override
  // defaultHandler, so, good enough.
  //
  // References:
  //   - https://github.com/nuxt/nuxt/issues/15432
  //   - https://nuxt.com/docs/getting-started/error-handling
  //
  showError(e)
  throw createError(e)
}

function jsonSerializer(data) {
  return JSON.stringify(data)
}

function queryStringSerializer(params) {
  return qs.stringify(params, { arrayFormat: 'repeat' })
}
