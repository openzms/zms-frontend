# zms-frontend (OpenZMS web UI)

The [OpenZMS software](https://gitlab.flux.utah.edu/openzms) is a prototype
automatic spectrum-sharing management system for radio dynamic zones.
OpenZMS provides mechanisms to share electromagnetic (radio-frequency)
spectrum between experimental or test systems and existing spectrum users,
and between multiple experimental systems.  We are building and deploying
OpenZMS within the context of the POWDER wireless testbed in Salt Lake City,
Utah, part of the NSF-sponsored Platforms for Advanced Wireless Research
program, to create [POWDER-RDZ](https://rdz.powderwireless.net).

This repository contains a web-based front end for OpenZMS services.  It relies
on multiple backend services:

  * [zms-identity](https://gitlab.flux.utah.edu/openzms/zms-identity): an
    identity and token service.  Provides implementations of primary,
    high-level OpenZMS abstractions like Users and Elements, and role-based
    access control.

  * [zms-zmc](https://gitlab.flux.utah.edu/openzms/zms-zmc): the core zone
    management controller.  Provides the ZEAL interface, which allows zone
    elements (participating organizations who contribute, monitor, and use
    spectrum in the zone) to programmatically interact with (e.g. reserve
    spectrum) and receive updates (e.g. interference reports, etc) from the
    zone.

  * [zms-dst](https://gitlab.flux.utah.edu/openzms/zms-dst): the OpenZMS
    Digital Spectrum Twin service zone, which provides observation (metrics,
    samples, etc) and propagation simulation services and query support.

## Quick dev mode start

Install the dependencies:

```bash
# npm
npm install
```

The [zms-identity](https://gitlab.flux.utah.edu/openzms/zms-identity) service
supports direct credential login, but if you want to enable identity
providers for single sign-on login and user account creation, you should
update `.env`:

```bash
# To create a GitHub OAuth App, go to https://github.com/settings/applications/new .
# Enter `http://localhost:3000` for `Homepage URL` and `Authorization
# callback URL`; set other fields as desired.  This will create a new OAuth app
# with a `Client Id`, which you will paste below.  Then, click to generate a new
# `Client Secret`, and copy that below as well.
GITHUB_CLIENT_ID=
GITHUB_CLIENT_SECRET=

# TBD.
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=

# TBD.
CILOGON_CLIENT_ID=
CILOGON_CLIENT_SECRET=
``

Start the development server on `http://localhost:3000`:

```bash
npm run dev -- -o
```
