export default defineNuxtPlugin(({ $userStore, $clientStore }) => {
  return {
    provide: {
      fetch: async (resource, options) => {
        if (resource.startsWith('/zms/')) {
          const headers = new Headers()
          if (options && options.headers && typeof options.headers == 'object') {
            var it = null
            if (typeof options.headers.forEach !== 'undefined')
              it = options.headers.entries
            else
              it = Object.entries(options.headers)
            for (const [k, v] of it) {
              headers.set(k, v)
            }
          }
          headers.set('X-Api-Elaborate', 'true')
          headers.set('X-Api-Ask-Edit', 'true')

          if (false && !$userStore.token && resource.startsWith('/zms/identity/tokens')) {
            headers.set('X-Api-Token', process.env.SERVICE_API_TOKEN)
          }
          else if ($clientStore.is_admin && $userStore.admin_token) {
            headers.set('X-Api-Token', $userStore.admin_token.token)
          }
          else if ($userStore.token) {
            headers.set('X-Api-Token', $userStore.token.token)
          }

          options = { ...options, headers }
        }
        return $fetch(resource, options)
      }
    }
  }
})
