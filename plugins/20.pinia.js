import { userStore } from '~/stores/user'
import { clientStore } from '~/stores/client'
import { eventStore } from '~/stores/event'
import { zoneStore } from '~/stores/zone'
import { adminStore } from '~/stores/admin'
import { filtersStore } from '~/stores/filters'

export default defineNuxtPlugin(({ $pinia }) => {
  return {
    provide: {
      userStore: userStore($pinia),
      clientStore: clientStore($pinia),
      eventStore: eventStore($pinia),
      zoneStore: zoneStore($pinia),
      adminStore: adminStore($pinia),
      filtersStore: filtersStore($pinia)
    }
  }
})
