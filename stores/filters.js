
import { defineStore } from 'pinia'

export const filtersStore = defineStore('filters', {
    state: () => ({
        filters: {
            component: "",
            filters: null,
            options: null,
        },
    }),
    persist: {
        storage: piniaPluginPersistedstate.localStorage(),
    },
    actions: {
        dump() {
            console.log("filtersStore", this.filters);
        },
        haveFilters(component) {
            return this.filters.component == component;
        },
        getFilters(component) {
            return this.filters;
        },
        setFilters(component, filters, options) {
            this.filters.component = component;
            this.filters.filters = filters;
            this.filters.options = options;
        },
        clearFilters(component) {
            this.filters.component = "";
            this.filters.filters = null;
            this.filters.options = null;
        },
    },
})
