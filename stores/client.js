
import { defineStore } from 'pinia'

export const clientStore = defineStore('client', {
    state: () => ({
        is_admin: false,
    }),
    persist: {
        storage: piniaPluginPersistedstate.localStorage(),
    },
    actions: {
        async logout() {
            this.$reset()
        },
        async setAdmin(mode) {
            this.is_admin = mode
            console.log("setAdmin", mode)
            this.$persist()
        }
    },
})
