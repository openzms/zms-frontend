import * as echarts from 'echarts'
import * as _ from 'underscore';
import Papa from 'papaparse';
import moment from 'moment';

// Changes the date.
var debug = 1;
var colors = ['#5470c6', '#91cc75', '#ee6666', '#73c0de', '#3ba272', '#fc8452',
              '#9a60b4', '#ea7ccc'];

/*
 * So we can change the color of individual points dynamically, but
 * nothing else (at least in a "scatter" graph). But there are
 * MarkPoints, which can be tailored for each point. So this bit of
 * hackery, draws a border around each violation/interference dot,
 * each of which has the color of their monitor. We create a dot
 * of the same size, but transparent, with a 2 pixel border around
 * it. Perhaps clever, perhaps not. 
 */
function addViolationPoints(series, data)
{
    data.forEach(function (d) {
        if (d.violation || d.interference) {
            var color = (d.violation ? "red" : "black");
            var point = {
                xAxis: d.created_at,
                yAxis: d.kurtosis,
                itemStyle: {
                    // note: alpha is supported
                    color: '#00000000',
                    borderColor: color,
                    borderWidth: 2,
                },
                label: {
                    show: false,
                },
                symbol: 'circle',
                symbolSize: 10,
                data: d,
            };
            series.markPoint.data.push(point);
        }
    });
}

/*
 * Each monitor is an object in the "series" slot of the chart options.
 * Each has its own color and name (for the legend). Each time we see
 * an observation for a new monitor, add a new one of these to the series.
 */
function createSeriesObject(index, data, monitor)
{
    var series = {
        name: monitor.name,
        datasetIndex: index,
        type: 'scatter',
        encode: {
            x: "created_at",
            y: "kurtosis",
        },
        itemStyle: {
            color: function(param) {
                return colors[index];
            },
        },
        markPoint: {
            data: [],
            animation: false,
        },
    };
    addViolationPoints(series, data);
    return series;
}

/*
 * From each new observation, need to unwrap the data by parsing the
 * CSV file into an array of objects.  This might switch to using the
 * annotations at some point.
 */
function unwrapObservationData(observation, monitor)
{
    var papaResults =
        Papa.parse(atob(observation.data),
                   {
                       dynamicTyping : true,
                       skipEmptyLines: true,
                       header : true,
                   });
    var data = papaResults.data;

    // Housekeeping on the data
    data.forEach(function (d) {
        // Need to convert the dates to objects.
        if (debug) {
            d.created_at = new Date();
        }
        else {
            d.created_at = new Date(d.created_at);
        }
        // For the tooltip when there are multiple monitors
        if (monitor) {
            d["monitor"] = monitor.name;
        }
        // Violations are per data point, but interference is on the observation.
        d.interference = observation.interference;
    });
    return data;
}

export default function createKurtosisGraph(observation, monitor) {
    console.info("createKurtosisGraph", observation, monitor);

    // Clear old graph
    var mainDiv = document.querySelector(".kurtosis-graph-maingraph .echarts-container");
    if (mainDiv) {
        echarts.dispose(mainDiv);
    }
    
    // Show monitor name when we get a monitor.
    if (monitor) {
        var tippy = document
            .querySelector(".popover-content .tooltip-monitor").parentNode;
        tippy.style.display = null;
    }
    var data = unwrapObservationData(observation, monitor);

    /*
     * Both the "dataset" and the "series" slots of the chart options
     * are arrays. Each monitor has an associated entry in both.
     * Keep track of them here. Seed the first one, others are added
     * in the callback below.
     */
    var dataset = [
        {
            source : data
        }
    ];
    var series = [
        createSeriesObject(0, data, monitor)
    ];
    /*
     * Map from the monitor_id to the index in the above two arrays. When a
     * new observation comes in, find its index from the observation monitor_id
     */
    var datamap = {};
    datamap[observation.monitor_id] = 0;
    
    var chart = createGraph(dataset, series);

    // Return a function that allows adding new data as it comes in.
    return function (observation, monitor) {
        var newdata = unwrapObservationData(observation, monitor);
        var options = chart.getOption();
        
        console.info("Add new data", newdata)
        if (!_.has(datamap, observation.monitor_id)) {
            var index = _.size(datamap);
            datamap[observation.monitor_id] = index;
            // New dataset.
            dataset.push({
                source : newdata
            });
            series.push(createSeriesObject(index, newdata, monitor));
        }
        else {
            var index  = datamap[observation.monitor_id];
            // Append new data
            var data  = dataset[index].source;
            dataset[index].source = data.concat(newdata);
            // Update the series for new violations.
            addViolationPoints(series[index], newdata);
        }
        console.info(dataset, series);
        
        chart.setOption({
            dataset : dataset,
            series  : series,
        });
    };
}

function updateToolTip(tip, field, value)
{
    var td = tip.querySelector(field);
    td.innerHTML = value;
}

function createGraph(dataset, series)
{
    var graph = document.querySelector(".kurtosis-graph-maingraph");
    graph.style.display = null;
    
    // Create the echarts instance
    var myChart = echarts.init(graph.querySelector(".echarts-container"));

    // Need to expicitly resize.
    window.onresize = function() {
        myChart.resize();
    };    
   
    // We clone the tooltip from the callback below.
    var tip = document.querySelector(".kurtosis-graph-maingraph .popover-content");

    window.mychart = myChart;

    // Draw the chart
    myChart.setOption({
        dataset: dataset,
        series: series,
        color: colors,
        legend: {},
        animation: false,
        grid: {
            top: 40,
            bottom: 80,
            left: 60,
            right: 10,
        },
        tooltip: {
            trigger: "item",
            padding: 2,
            formatter: function (param) {
                //console.info(param);
                var tipc = tip.cloneNode(true);
                var data;
                if (param.componentType == 'markPoint') {
                    data = param.data.data;
                }
                else {
                    data = param.data;
                }
                updateToolTip(tipc, ".tooltip-frequency", data.frequency.toFixed(3));
                updateToolTip(tipc, ".tooltip-power", data.power.toFixed(3));
                updateToolTip(tipc, ".tooltip-center", data.center_freq.toFixed(3));
                updateToolTip(tipc, ".tooltip-floor", data.abovefloor.toFixed(3));
                updateToolTip(tipc, ".tooltip-kurtosis", data.kurtosis.toFixed(3));
                updateToolTip(tipc, ".tooltip-violation",
                              data.violation ? "Yes" : "No");
                updateToolTip(tipc, ".tooltip-interference",
                              data.interference ? "Yes" : "No");
                updateToolTip(tipc, ".tooltip-created",
                              moment(data.created_at).format("lll"));
                if (_.has(data, "monitor")) {
                    updateToolTip(tipc, ".tooltip-monitor", data.monitor);
                }
                return tipc;
            },
            shadowColor: "rgba(0, 0, 0, 0)",
            shadowBlur: 0,  
            borderWidth: 1,
            borderColor: "grey",
        },
        xAxis: {
            name: "Date",
            nameLocation: "middle",
            nameGap: 20,
            max: "dataMax",
            min: "dataMin",
            type: "time",
        },
        yAxis: {
            name: "Kurtosis",
            nameLocation: "middle",
            nameGap: 40,
            max: "dataMax",
            min: "dataMin",
            axisLabel: {
                formatter: function (value) {
                    return value.toFixed(2);
                },
            },
        },
        dataZoom: [
            {
                type: 'inside',
            },
            {
                type: 'slider',
                height: 40,
            },
        ],
    });
    return myChart;
}

