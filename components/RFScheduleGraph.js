import * as echarts from 'echarts'
import * as _ from 'underscore';
import moment from 'moment';
import { getElementNameById } from '~/lib/helpers'

export default function createScheduleGraph(args) {
    console.info("createScheduleGraph", args);
    createGraph(args);
}

function createGraph(args)
{
    var callback    = args.callback;
    var grants      = args.grants;
    var claims      = args.claims;
    var showPending = args.showPending;
    
    var graph = document.querySelector(".schedule-graph");
    graph.style.display = null;
    
    // Create the echarts instance
    var myChart = echarts.init(graph.querySelector(".echarts-container"));

    // Need to expicitly resize.
    window.onresize = function() {
        myChart.resize();
    };

    // Debugging
    window.mychart = myChart;

    // We clone the tooltip from the callback below.
    var tip = document.querySelector(".schedule-graph .popover-content");
    function updateToolTip(tip, field, value)
    {
        var td = tip.querySelector(field);
        td.innerHTML = value;
    }

    /*
     * Click handler for the boxes
     */
    myChart.on('click', function(params) {
        console.info("click", params);
        var type = params.data.mytype;
        var item = params.data.myitem;
        callback(type, item);
    });

    /*
     * Convert claims and grants into series data.
     */
    var data = [];
    // In case we run into a grant or claim with no expires.
    var maxend = moment(0).utc();
    
    if (grants) {
        for (var i = 0; i < grants.length; i++) {
            var grant = grants[i];
            if (grant.status == "revoked" ||
                (grant.status == "pending" && !showPending)) {
                continue;
            }
            var min   = grant.constraints[0].constraint.min_freq / 1000000.0;
            var max   = grant.constraints[0].constraint.max_freq / 1000000.0;
            var start = grant.starts_at;
            var ends  = grant.expires_at;
            var prio  = grant.priority;

            if (ends && moment(ends).isAfter(maxend)) {
                maxend = ends;
            }
            
            data.push({
	        value: [
	            start,
	            ends,
	            min,
	            max,
                    prio,
	        ],
                itemStyle: {
                    color: 'rgba(255, 173, 100, 0.8)'
                },
                mytype: "grant",
                myitem: grant,
            });
        }
    }
    if (claims) {
        for (var i = 0; i < claims.length; i++) {
            var claim  = claims[i];
            var grant  = claim.grant;
            if (grant.status == "pending" && !showPending) {
                continue;
            }
            var min    = grant.constraints[0].constraint.min_freq / 1000000.0;
            var max    = grant.constraints[0].constraint.max_freq / 1000000.0;
            var start  = grant.starts_at;
            var ends   = grant.expires_at;
            var prio   = grant.priority;

            if (ends && moment(ends).isAfter(maxend)) {
                maxend = ends;
            }
            
            data.push({
	        value: [
	            start,
	            ends,
	            min,
	            max,
                    prio,
	        ],
                itemStyle: {
                    color: 'rgba(255, 173, 177, 0.8)'
                },
                mytype: "claim",
                myitem: claim,
            });
        }
    }
    // Look for any with no expiration.
    for (var i = 0; i < data.length; i++) {
        if (!data[i].value[1]) {
            data[i].value[1] = maxend;
        }
    }
    
    console.info("grants/claims", data);

    var options = {
        animation: false,
        grid: {
            top: 10,
            bottom: 80,
            left: 50,
            right: 10,
        },
        xAxis: {
	    type: "time",
        },
        yAxis: {
	    type: "value",
            scale: true,
        },
        dataZoom: [
            {
                type: 'inside',
                filterMode: 'weakFilter',
                startValue: moment().valueOf(),
            },
            {
                type: 'slider',
                filterMode: 'weakFilter',
                startValue: moment().valueOf(),
                height: 40,
            },
        ],
        tooltip: {
            padding: 2,
            formatter: function (params) {
                //console.info("tooltip", params);
                var data  = params.data.value;
                var tipc  = tip.cloneNode(true);
                var item  = params.data.myitem;
                var exclusive = "No";
                var grant = item;

                if (params.data.mytype == "claim") {
                    grant = item.grant;
                }
                
                updateToolTip(tipc, ".tooltip-start", moment(grant.starts_at).format("lll"))
                if (grant.expires_at) {
                    updateToolTip(tipc, ".tooltip-end", moment(grant.expires_at).format("lll"));
                }
                else {
                    updateToolTip(tipc, ".tooltip-end", "");
                }
                updateToolTip(tipc, ".tooltip-low", grant.constraints[0].constraint.min_freq);
                updateToolTip(tipc, ".tooltip-high", grant.constraints[0].constraint.max_freq);
                updateToolTip(tipc, ".tooltip-eirp", grant.constraints[0].constraint.max_eirp);
                updateToolTip(tipc, ".tooltip-status", grant.status);
                if (grant.constraints[0].constraint.exclusive) {
                    exclusive = "Yes";
                }
                updateToolTip(tipc, ".tooltip-element", getElementNameById(item.element_id));
                updateToolTip(tipc, ".tooltip-name", item.name);
                updateToolTip(tipc, ".tooltip-exclusive", exclusive);
                return tipc;
            },
            shadowColor: "rgba(0, 0, 0, 0)",
            shadowBlur: 0,  
            borderWidth: 1,
            borderColor: "grey",
        },
        series: [
	    {
	        type: 'custom',
	        dimensions: ["Start", "End", "Low", "High"],
	        renderItem: function (params, api) {
		    var start = api.value(0);
		    var end   = api.value(1);
		    var low   = api.value(2);
		    var high  = api.value(3);

		    var topleft  = api.coord([start, high]);
		    var botright = api.coord([end, low]);
		    var x0 = topleft[0];
		    var y0 = topleft[1];
		    var x1 = botright[0];
		    var y1 = botright[1];
                    //console.info(low, high, start, end);
		    //console.info(x0, y0, x1, y1);

                    var rectShape = echarts.graphic.clipRectByRect(
                        {
			    x: x0,
			    y: y0,
			    width: x1 - x0,
			    height: y1 - y0
                        },
                        {
                            x: params.coordSys.x,
                            y: params.coordSys.y,
                            width: params.coordSys.width,
                            height: params.coordSys.height
                        }
                    );
                    return (
                        rectShape && {
                            type: 'rect',
                            transition: ['shape'],
                            shape: rectShape,
                            style: api.style()
                        }
                    );
	        },
	        label: {
		    show: false,
		    position: 'top'
	        },
	        encode: {
		    x: [0, 1],
		    y: [2, 3],
	        },
	        data: data
	    }
        ]
    };
    console.info("chart options", options);
    myChart.setOption(options);
}
